# Python 3.7 Package Creator for AWS Lambda

<a href="https://www.buymeacoffee.com/migfru">Buy me a coffee!![Buy me a coffee!](https://www.buymeacoffee.com/assets/img/BMC-btn-logo.svg)</a>

## Quick start
`docker run --rm -v $(pwd):/package migfru/lambdazipper MYPACKAGE`

**Example:**

`docker run --rm -v $(pwd):/package migfru/lambdazipper numpy`

Result is `numpy.zip` that can be added as a [Lambda Layer](https://aws.amazon.com/blogs/aws/new-for-aws-lambda-use-any-programming-language-and-share-common-components/) or extracted into your existing Lambda deployment package.



---
### Testing dependencies

### This project consists of two key components
1. Docker environment mimicing AWS Lambda environment. If you are looking for an advanced Lambda environment replication, take a look at [lambci/docker-lambda](https://github.com/lambci/docker-lambda).
2. Packaging script to build AWS Lambda ready packages from Python 3.7 modules.

## Dockerfile for Lambda environment
Premade Docker image can be found on Docker Hub https://cloud.docker.com/repository/docker/migfru/lambdazipper
```
FROM amazonlinux:2017.03
RUN yum install -y git gcc openssl-devel bzip2-devel libffi-devel zip wget curl && yum clean all
RUN cd /opt
RUN wget https://www.python.org/ftp/python/3.7.9/Python-3.7.9.tgz
RUN tar xzf Python-3.7.9.tgz
RUN cd Python* && ./configure --enable-optimizations && make altinstall
RUN rm -f Python-3.7.9.tgz
RUN curl -O https://bootstrap.pypa.io/get-pip.py
Run export PATH=/usr/local/bin/:$PATH
RUN python3.7 get-pip.py --user

ADD package.sh /
ENTRYPOINT ["/package.sh"]
```

#### Build your own Docker image for finer control
```
docker build -t my-lambda .
```
