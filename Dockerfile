FROM amazonlinux:2.0.20210219.0
RUN yum install -y git gcc openssl-devel bzip2-devel libffi-devel zip wget curl && yum clean all
RUN cd /opt
RUN wget https://www.python.org/ftp/python/3.7.9/Python-3.7.9.tgz
RUN tar xzf Python-3.7.9.tgz
RUN cd Python* && ./configure --enable-optimizations && make altinstall
RUN rm -f Python-3.7.9.tgz
RUN curl -O https://bootstrap.pypa.io/get-pip.py
Run export PATH=/usr/local/bin/:$PATH
RUN python3.7 get-pip.py --user


ADD package.sh /
ENTRYPOINT ["/package.sh"]
